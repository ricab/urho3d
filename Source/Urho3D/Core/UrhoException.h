/*
 * UrhoException.hpp
 *
 *  Created on: 28/03/2017
 *      Author: ricab
 */

#ifndef URHOEXCEPTION_H_
#define URHOEXCEPTION_H_

#include "../Container/Str.h"

#include <boost/format.hpp>

#include <stdexcept>
#include <string>

namespace Urho3D
{
  //////////////////////////////////////////////////////////////////////////////
  class URHO3D_API UrhoException: public std::runtime_error
  {
  public:
    explicit UrhoException(const char * what);
    explicit UrhoException(const std::string& what);
    explicit UrhoException(const String& what);
  };

  //////////////////////////////////////////////////////////////////////////////
  class URHO3D_API BadRepresentationException : public UrhoException
  {
  public:
    explicit BadRepresentationException(const char * value, const char * type);
    explicit BadRepresentationException(const String& value,
                                        const String& type);

    const String m_value;
    const String m_type;

  private:
      static boost::format ms_msg_format;
  };

  //////////////////////////////////////////////////////////////////////////////
  class URHO3D_API UnknownAttributeException : public UrhoException
  {
  public:
    explicit UnknownAttributeException(const char * attr,
                                       const char * elem,
                                       const char * source);
    explicit UnknownAttributeException(const String& attr,
                                       const String& elem,
                                       const String& source);

  private:
    static boost::format ms_msg_format;
  };

} /* namespace Urho3D */

////////////////////////////////////////////////////////////////////////////////
inline Urho3D::UrhoException::UrhoException(const char * what)
  : std::runtime_error{what}
{}

////////////////////////////////////////////////////////////////////////////////
inline Urho3D::UrhoException::UrhoException(const std::string& what)
  : std::runtime_error{what}
{}

////////////////////////////////////////////////////////////////////////////////
inline Urho3D::UrhoException::UrhoException(const Urho3D::String& what)
  : std::runtime_error{what.CString()}
{}

#endif /* URHOEXCEPTION_H_ */
