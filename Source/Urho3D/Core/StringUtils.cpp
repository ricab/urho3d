//
// Copyright (c) 2008-2017 the Urho3D project.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../Precompiled.h"

#include "../Core/StringUtils.h"
#include "../Core/UrhoException.h"

#include <cstdio>
#include <string>
#include <typeinfo>

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

#include "../DebugNew.h"

namespace
{
  bool is_separator(const char& c)
{
    return c == ',' || boost::algorithm::is_space()(c);
    }

  template <typename T>
  T lexcast(const char * source)
    {
    try
        {
      return boost::lexical_cast<T>(source);
        }
    catch(const boost::bad_lexical_cast& e)
        {
      throw Urho3D::BadRepresentationException{source, typeid(T).name()};
    }
  }
        }

namespace Urho3D
        {

static const String base64_chars =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz"
        "0123456789+/";

bool ToBool(const String& source)
{
    return ToBool(source.CString());
}

bool ToBool(const char* source)
{
  std::string simplified{source};
  boost::algorithm::to_lower(simplified);
  boost::algorithm::trim(simplified);

  if(simplified == "true" || simplified == "yes") // try true and yes
            return true;
  else if(simplified == "false" || simplified == "no" || simplified.empty()) /*
                                                     try false, no, and empty */
    return false;

  // falback to default conversion
  try
  {
    return boost::lexical_cast<bool>(source); // try default conversion
  }
  catch(const boost::bad_lexical_cast& original_exc)
  {
    try
    {
      return bool(boost::lexical_cast<int>(source)); // try conversion to int
    }
    catch(const boost::bad_lexical_cast&)
    {
      throw BadRepresentationException{source, typeid(bool).name()};// give up
    }
  }
}

int ToInt(const String& source, int base)
{
    return ToInt(source.CString(), base);
}

int ToInt(const char* source, int base)
{
    // Shield against runtime library assert by converting illegal base values to 0 (autodetect)
    if (base < 2 || base > 36)
        base = 0;

    if(base && base != 10)
      return (int)strtol(source, 0, base);

    return lexcast<int>(source);
}

long long ToInt64(const char* source, int base)
{
    // Shield against runtime library assert by converting illegal base values to 0 (autodetect)
    if (base < 2 || base > 36)
        base = 0;

    if(base && base != 10)
      return strtoll(source, 0, base);

    return lexcast<long long>(source);
}

long long ToInt64(const String& source, int base)
{
    return ToInt64(source.CString(), base);
}

unsigned ToUInt(const String& source, int base)
{
    return ToUInt(source.CString(), base);
}

unsigned ToUInt(const char* source, int base)
{
    if (base < 2 || base > 36)
        base = 0;

    if(base && base != 10)
      return (unsigned)strtoul(source, 0, base);

    return lexcast<unsigned>(source);
}

unsigned long long ToUInt64(const char* source, int base)
{
    // Shield against runtime library assert by converting illegal base values to 0 (autodetect)
    if (base < 2 || base > 36)
        base = 0;

    if(base && base != 10)
      return strtoull(source, 0, base);

    return lexcast<unsigned long long>(source);
}

unsigned long long ToUInt64(const String& source, int base)
{
    return ToUInt64(source.CString(), base);
}

float ToFloat(const String& source)
{
    return ToFloat(source.CString());
}

float ToFloat(const char* source)
{
  return lexcast<float>(source);
}

double ToDouble(const String& source)
{
    return ToDouble(source.CString());
}

double ToDouble(const char* source)
{
  return lexcast<double>(source);
}

Color ToColor(const String& source)
{
    return ToColor(source.CString());
}

Color ToColor(const char* source)
{
  if(source && *source) // not empty
  {
    std::vector<std::string> split;
    boost::algorithm::split(split, source, is_separator,
                            boost::algorithm::token_compress_on);
    try
    {
      if(split.size() == 3)
        return Color{boost::lexical_cast<float>(split[0]),
                     boost::lexical_cast<float>(split[1]),
                     boost::lexical_cast<float>(split[2])};
      else if(split.size() == 4)
        return Color{boost::lexical_cast<float>(split[0]),
                     boost::lexical_cast<float>(split[1]),
                     boost::lexical_cast<float>(split[2]),
                     boost::lexical_cast<float>(split[3])};
    } catch(const boost::bad_lexical_cast&) {/* handle below */}
  }

  throw BadRepresentationException{source, typeid(Color).name()}; // failure
  return Color{}; // never reached
}

IntRect ToIntRect(const String& source)
{
    return ToIntRect(source.CString());
}

IntRect ToIntRect(const char* source)
{
  if(source && *source) // not empty
  {
    std::vector<std::string> split;
    boost::algorithm::split(split, source, is_separator,
                            boost::algorithm::token_compress_on);

    try
    {
      if(split.size() == 4)
        return IntRect{boost::lexical_cast<int>(split[0]),
                       boost::lexical_cast<int>(split[1]),
                       boost::lexical_cast<int>(split[2]),
                       boost::lexical_cast<int>(split[3])};
    } catch(const boost::bad_lexical_cast&) {/* handle below */}
  }

  throw BadRepresentationException{source, typeid(IntRect).name()}; // failure
  return IntRect::ZERO; // never reached
}

IntVector2 ToIntVector2(const String& source)
{
    return ToIntVector2(source.CString());
}

IntVector2 ToIntVector2(const char* source)
{
  if(source && *source) // not empty
  {
    std::vector<std::string> split;
    boost::algorithm::split(split, source, is_separator,
                            boost::algorithm::token_compress_on);

    try
    {
      if(split.size() == 2)
        return IntVector2{boost::lexical_cast<int>(split[0]),
                          boost::lexical_cast<int>(split[1])};
    } catch(const boost::bad_lexical_cast&) {/* handle below */}
  }

  throw BadRepresentationException{source, typeid(IntVector2).name()}; //failure
  return IntVector2::ZERO; // never reached
}

IntVector3 ToIntVector3(const String& source)
{
    return ToIntVector3(source.CString());
}

IntVector3 ToIntVector3(const char* source)
{
  if(source && *source) // not empty
  {
    std::vector<std::string> split;
    boost::algorithm::split(split, source, is_separator,
                            boost::algorithm::token_compress_on);

    try
    {
      if(split.size() == 2)
        return IntVector3{boost::lexical_cast<int>(split[0]),
                          boost::lexical_cast<int>(split[1]),
                          boost::lexical_cast<int>(split[2])};
    } catch(const boost::bad_lexical_cast&) {/* handle below */}
  }

  throw BadRepresentationException{source, typeid(IntVector3).name()}; //failure
  return IntVector3::ZERO; // never reached
}

Rect ToRect(const String& source)
{
    return ToRect(source.CString());
}

Rect ToRect(const char* source)
{
  if(source && *source) // not empty
  {
    std::vector<std::string> split;
    boost::algorithm::split(split, source, is_separator,
                            boost::algorithm::token_compress_on);

    try
    {
      if(split.size() == 4)
        return Rect{boost::lexical_cast<float>(split[0]),
                    boost::lexical_cast<float>(split[1]),
                    boost::lexical_cast<float>(split[2]),
                    boost::lexical_cast<float>(split[3])};
    } catch(const boost::bad_lexical_cast&) {/* handle below */}
  }

  throw BadRepresentationException{source, typeid(Rect).name()}; // failure
  return Rect::ZERO; // never reached
}

Quaternion ToQuaternion(const String& source)
{
    return ToQuaternion(source.CString());
}

Quaternion ToQuaternion(const char* source)
{
  if(source && *source) // not empty
  {
    std::vector<std::string> split;
    boost::algorithm::split(split, source, is_separator,
                            boost::algorithm::token_compress_on);

    try
    {
      if(split.size() == 4)
        return Quaternion{boost::lexical_cast<float>(split[0]),
                          boost::lexical_cast<float>(split[1]),
                          boost::lexical_cast<float>(split[2]),
                          boost::lexical_cast<float>(split[3])};
      else if(split.size() == 3)
          return Quaternion{boost::lexical_cast<float>(split[0]),
                            boost::lexical_cast<float>(split[1]),
                            boost::lexical_cast<float>(split[2])};
    } catch(const boost::bad_lexical_cast&) {/* handle below */}
    }

  throw BadRepresentationException{source, typeid(Quaternion).name()}; //failure
  return Quaternion::IDENTITY; // never reached
}

Vector2 ToVector2(const String& source)
{
    return ToVector2(source.CString());
}

Vector2 ToVector2(const char* source)
{
  if(source && *source) // not empty
  {
    std::vector<std::string> split;
    boost::algorithm::split(split, source, is_separator,
                            boost::algorithm::token_compress_on);

    try
    {
      if(split.size() == 2)
        return Vector2{boost::lexical_cast<float>(split[0]),
                       boost::lexical_cast<float>(split[1])};
    } catch(const boost::bad_lexical_cast&) {/* handle below */}
  }

  throw BadRepresentationException{source, typeid(Vector2).name()}; // failure
  return Vector2::ZERO; // never reached
}

Vector3 ToVector3(const String& source)
{
    return ToVector3(source.CString());
}

Vector3 ToVector3(const char* source)
{
  if(source && *source) // not empty
  {
    std::vector<std::string> split;
    boost::algorithm::split(split, source, is_separator,
                            boost::algorithm::token_compress_on);

    try
    {
    if(split.size() == 3)
        return Vector3{boost::lexical_cast<float>(split[0]),
                       boost::lexical_cast<float>(split[1]),
                       boost::lexical_cast<float>(split[2])};
    } catch(const boost::bad_lexical_cast&) {/* handle below */}
  }

  throw BadRepresentationException{source, typeid(Vector3).name()}; // failure
  return Vector3::ZERO; // never reached
}

Vector4 ToVector4(const String& source, bool allowMissingCoords)
{
    return ToVector4(source.CString(), allowMissingCoords);
}

Vector4 ToVector4(const char* source, bool allowMissingCoords)
{
  if(!source || !*source) // empty
  {
    if(allowMissingCoords)
      return {};
    throw BadRepresentationException{source, typeid(Vector4).name()}; // failure
  }

  std::vector<std::string> split;
  boost::algorithm::split(split, source, is_separator,
                          boost::algorithm::token_compress_on);
  const auto size = split.size();

  try
    {
    if(size < 5 && (allowMissingCoords || size == 4))
    {
      Vector4 ret;
      switch(size)// @suppress("No break at the end of case")
      {
      case 4: ret.w_ = boost::lexical_cast<float>(split[3]);
        // no break
      case 3: ret.z_ = boost::lexical_cast<float>(split[2]);
        // no break
      case 2: ret.y_ = boost::lexical_cast<float>(split[1]);
        // no break
      case 1: ret.x_ = boost::lexical_cast<float>(split[0]);
      }
        return ret;
    }
  } catch(const boost::bad_lexical_cast&) {/* handle below */}

  throw BadRepresentationException{source, typeid(Vector4).name()}; // failure
  return Vector4::ZERO; // never reached
}

Variant ToVectorVariant(const String& source)
{
    return ToVectorVariant(source.CString());
}

Variant ToVectorVariant(const char* source)
{
  if(source && *source) // not empty
  {
    std::vector<std::string> split;
    boost::algorithm::split(split, source, is_separator,
                            boost::algorithm::token_compress_on);

    try
    {
      switch(split.size())
      {
      case 1: return Variant{ToFloat(source)};
      case 2: return Variant{ToVector2(source)};
      case 3: return Variant{ToVector3(source)};
      case 4: return Variant{ToVector4(source)};
      case 9: return Variant{ToMatrix3(source)};
      case 12: return Variant{ToMatrix3x4(source)};
      case 16: return Variant{ToMatrix4(source)};
      }
    } catch(const boost::bad_lexical_cast&) {/* handle below */}
    }

  throw BadRepresentationException{source, typeid(Variant).name()}; // failure
  return Variant{}; // never reached
}

Matrix3 ToMatrix3(const String& source)
{
    return ToMatrix3(source.CString());
}

Matrix3 ToMatrix3(const char* source)
{
  std::vector<std::string> split;
  boost::algorithm::split(split, source, is_separator,
                          boost::algorithm::token_compress_on);

  try
  {
    if(split.size() == 9)
      return Matrix3{boost::lexical_cast<float>(split[0]),
                     boost::lexical_cast<float>(split[1]),
                     boost::lexical_cast<float>(split[2]),
                     boost::lexical_cast<float>(split[3]),
                     boost::lexical_cast<float>(split[4]),
                     boost::lexical_cast<float>(split[5]),
                     boost::lexical_cast<float>(split[6]),
                     boost::lexical_cast<float>(split[7]),
                     boost::lexical_cast<float>(split[8])};
  } catch(const boost::bad_lexical_cast&) {/* handle below */}

  throw BadRepresentationException{source, typeid(Matrix3).name()}; // failure
  return Matrix3::ZERO; // never reached
}

Matrix3x4 ToMatrix3x4(const String& source)
{
    return ToMatrix3x4(source.CString());
}

Matrix3x4 ToMatrix3x4(const char* source)
{
  std::vector<std::string> split;
  boost::algorithm::split(split, source, is_separator,
                          boost::algorithm::token_compress_on);

  try
  {
    if(split.size() == 12)
      return Matrix3x4{boost::lexical_cast<float>(split[0]),
                       boost::lexical_cast<float>(split[1]),
                       boost::lexical_cast<float>(split[2]),
                       boost::lexical_cast<float>(split[3]),
                       boost::lexical_cast<float>(split[4]),
                       boost::lexical_cast<float>(split[5]),
                       boost::lexical_cast<float>(split[6]),
                       boost::lexical_cast<float>(split[7]),
                       boost::lexical_cast<float>(split[8]),
                       boost::lexical_cast<float>(split[9]),
                       boost::lexical_cast<float>(split[10]),
                       boost::lexical_cast<float>(split[11])};
  } catch(const boost::bad_lexical_cast&) {/* handle below */}

  throw BadRepresentationException{source, typeid(Matrix3x4).name()}; // failure
  return Matrix3x4::ZERO; // never reached
}

Matrix4 ToMatrix4(const String& source)
{
    return ToMatrix4(source.CString());
}

Matrix4 ToMatrix4(const char* source)
{
  std::vector<std::string> split;
  boost::algorithm::split(split, source, is_separator,
                          boost::algorithm::token_compress_on);

  try
  {
    if(split.size() == 16)
      return Matrix4{boost::lexical_cast<float>(split[0]),
                     boost::lexical_cast<float>(split[1]),
                     boost::lexical_cast<float>(split[2]),
                     boost::lexical_cast<float>(split[3]),
                     boost::lexical_cast<float>(split[4]),
                     boost::lexical_cast<float>(split[5]),
                     boost::lexical_cast<float>(split[6]),
                     boost::lexical_cast<float>(split[7]),
                     boost::lexical_cast<float>(split[8]),
                     boost::lexical_cast<float>(split[9]),
                     boost::lexical_cast<float>(split[10]),
                     boost::lexical_cast<float>(split[11]),
                     boost::lexical_cast<float>(split[12]),
                     boost::lexical_cast<float>(split[13]),
                     boost::lexical_cast<float>(split[14]),
                     boost::lexical_cast<float>(split[15])};
  } catch(const boost::bad_lexical_cast&) {/* handle below */}

  throw BadRepresentationException{source, typeid(Matrix4).name()}; // failure
  return Matrix4::ZERO; // never reached
}

String ToString(void* value)
{
    return ToStringHex((unsigned)(size_t)value);
}

String ToStringHex(unsigned value)
{
    char tempBuffer[CONVERSION_BUFFER_LENGTH];
    sprintf(tempBuffer, "%08x", value);
    return String(tempBuffer);
}

void BufferToString(String& dest, const void* data, unsigned size)
{
    // Precalculate needed string size
    const unsigned char* bytes = (const unsigned char*)data;
    unsigned length = 0;
    for (unsigned i = 0; i < size; ++i)
    {
        // Room for separator
        if (i)
            ++length;

        // Room for the value
        if (bytes[i] < 10)
            ++length;
        else if (bytes[i] < 100)
            length += 2;
        else
            length += 3;
    }

    dest.Resize(length);
    unsigned index = 0;

    // Convert values
    for (unsigned i = 0; i < size; ++i)
    {
        if (i)
            dest[index++] = ' ';

        if (bytes[i] < 10)
        {
            dest[index++] = '0' + bytes[i];
        }
        else if (bytes[i] < 100)
        {
            dest[index++] = (char)('0' + bytes[i] / 10);
            dest[index++] = (char)('0' + bytes[i] % 10);
        }
        else
        {
            dest[index++] = (char)('0' + bytes[i] / 100);
            dest[index++] = (char)('0' + bytes[i] % 100 / 10);
            dest[index++] = (char)('0' + bytes[i] % 10);
        }
    }
}

void StringToBuffer(PODVector<unsigned char>& dest, const String& source)
{
    StringToBuffer(dest, source.CString());
}

void StringToBuffer(PODVector<unsigned char>& dest, const char* source)
{
  if(!source || !*source) // empty
        return;

  std::vector<std::string> split;
  boost::algorithm::split(split, source, is_separator,
                          boost::algorithm::token_compress_on);

  dest.Resize(split.size());
  try
    {
    for(size_t i = 0; i < split.size(); ++i)
      dest[i] = static_cast<unsigned char>(ToUInt(split[i].c_str()));
        }
  catch(const boost::bad_lexical_cast&)
        {
    throw BadRepresentationException{source,
      typeid(PODVector<unsigned char>).name()}; // failure
        }
}

unsigned GetStringListIndex(const String& value, const String* strings, unsigned defaultIndex, bool caseSensitive)
{
    return GetStringListIndex(value.CString(), strings, defaultIndex, caseSensitive);
}

unsigned GetStringListIndex(const char* value, const String* strings, unsigned defaultIndex, bool caseSensitive)
{
    unsigned i = 0;

    while (!strings[i].Empty())
    {
        if (!strings[i].Compare(value, caseSensitive))
            return i;
        ++i;
    }

    return defaultIndex;
}

unsigned GetStringListIndex(const char* value, const char** strings, unsigned defaultIndex, bool caseSensitive)
{
    unsigned i = 0;

    while (strings[i])
    {
        if (!String::Compare(value, strings[i], caseSensitive))
            return i;
        ++i;
    }

    return defaultIndex;
}

String ToString(const char* formatString, ...)
{
    String ret;
    va_list args;
    va_start(args, formatString);
    ret.AppendWithFormatArgs(formatString, args);
    va_end(args);
    return ret;
}

bool IsAlpha(unsigned ch)
{
    return ch < 256 ? isalpha(ch) != 0 : false;
}

bool IsDigit(unsigned ch)
{
    return ch < 256 ? isdigit(ch) != 0 : false;
}

unsigned ToUpper(unsigned ch)
{
    return (unsigned)toupper(ch);
}

unsigned ToLower(unsigned ch)
{
    return (unsigned)tolower(ch);
}

String GetFileSizeString(unsigned long long memorySize)
{
    static const char* memorySizeStrings = "kMGTPE";

    String output;

    if (memorySize < 1024)
    {
        output = String(memorySize) + " b";
    }
    else
    {
        const int exponent = (int)(log((double)memorySize) / log(1024.0));
        const double majorValue = ((double)memorySize) / pow(1024.0, exponent);
        char buffer[64];
        memset(buffer, 0, 64);
        sprintf(buffer, "%.1f", majorValue);
        output = buffer;
        output += " ";
        output += memorySizeStrings[exponent - 1];
    }

    return output;
}

// Implementation of base64 decoding originally by Ren� Nyffenegger.
// Modified by Konstantin Guschin and Lasse Oorni

/*
base64.cpp and base64.h

Copyright (C) 2004-2017 Ren� Nyffenegger

This source code is provided 'as-is', without any express or implied
warranty. In no event will the author be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this source code must not be misrepresented; you must not
claim that you wrote the original source code. If you use this source code
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original source code.

3. This notice may not be removed or altered from any source distribution.

Ren� Nyffenegger rene.nyffenegger@adp-gmbh.ch

*/

static inline bool IsBase64(char c) {
    return (isalnum(c) || (c == '+') || (c == '/'));
}

PODVector<unsigned char> DecodeBase64(String encodedString) 
{
    int inLen = encodedString.Length();
    int i = 0;
    int j = 0;
    int in_ = 0;
    unsigned char charArray4[4], charArray3[3];
    PODVector<unsigned char> ret;

    while (inLen-- && (encodedString[in_] != '=') && IsBase64(encodedString[in_])) 
    {
        charArray4[i++] = encodedString[in_]; 
        in_++;

        if (i == 4)
        {
            for (i = 0; i < 4; i++)
                charArray4[i] = base64_chars.Find(charArray4[i]);

            charArray3[0] = (charArray4[0] << 2) + ((charArray4[1] & 0x30) >> 4);
            charArray3[1] = ((charArray4[1] & 0xf) << 4) + ((charArray4[2] & 0x3c) >> 2);
            charArray3[2] = ((charArray4[2] & 0x3) << 6) + charArray4[3];

            for (i = 0; (i < 3); i++)
                ret.Push(charArray3[i]);

            i = 0;
        }
    }

    if (i)
    {
        for (j = i; j <4; j++)
            charArray4[j] = 0;

        for (j = 0; j <4; j++)
            charArray4[j] = base64_chars.Find(charArray4[j]);

        charArray3[0] = (charArray4[0] << 2) + ((charArray4[1] & 0x30) >> 4);
        charArray3[1] = ((charArray4[1] & 0xf) << 4) + ((charArray4[2] & 0x3c) >> 2);
        charArray3[2] = ((charArray4[2] & 0x3) << 6) + charArray4[3];

        for (j = 0; (j < i - 1); j++) 
            ret.Push(charArray3[j]);
    }

    return ret;
}

}
