/*
 * UrhoException.cpp
 *
 *  Created on: 28/03/2017
 *      Author: ricab
 */

#include "Core/UrhoException.h"

namespace Urho3D
{
  //////////////////////////////////////////////////////////////////////////////
  boost::format BadRepresentationException::
  ms_msg_format{"Could not convert from string \"%s\" to type %s"};

  //////////////////////////////////////////////////////////////////////////////
  BadRepresentationException::BadRepresentationException(const char * value,
                                                         const char * type)
    : UrhoException{(ms_msg_format % value % type).str()}
    , m_value{value}
    , m_type{type}
  {}

  //////////////////////////////////////////////////////////////////////////////
  BadRepresentationException::BadRepresentationException(const String& value,
                                                         const String& type)
    : UrhoException{(ms_msg_format % value.CString() % type.CString()).str()}
  {}

  //////////////////////////////////////////////////////////////////////////////
  boost::format UnknownAttributeException::
  ms_msg_format{"Unknown attribute \"%s\" for type \"%s\", in %s"};

  //////////////////////////////////////////////////////////////////////////////
  UnknownAttributeException::UnknownAttributeException(const char * attr,
                                                       const char * elem,
                                                       const char * source)
    : UrhoException{(ms_msg_format % attr % elem % source).str()}
  {}

  //////////////////////////////////////////////////////////////////////////////
  UnknownAttributeException::UnknownAttributeException(const String& attr,
                                                       const String& elem,
                                                       const String& source)
    : UrhoException{(ms_msg_format % attr.CString()
                                   % elem.CString()
                                   % source.CString()).str()}
  {}

} /* namespace Urho3D */
