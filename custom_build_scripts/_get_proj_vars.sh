#!/usr/bin/env bash

# this should be sourced to derive basic project info into shell variables
# (e.g. what the source directory is; what the project's name is). The project
# name is exported so it can be used in subprocesses.
#
# Preconditions:
# - the calling script has to source this script to get the variables out
# - the variable THIS_DIR should be defined and point to the directory this
# script is in
# - this directory should be an immediate subdirectory of the project's root
# directory

# ---
# assuming this file is in a subdirectory of the project (e.g.
# custom_build_scripts), then the project dir should be:
SOURCE_DIR=$(dirname $THIS_DIR)
# and therefore the project's name:
export PROJ_NAME="${SOURCE_DIR##*/}"
# and the base directory, where builds are also located:
export BASE_DIR="$(dirname ${SOURCE_DIR})"
