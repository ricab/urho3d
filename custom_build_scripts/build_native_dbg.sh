#!/usr/bin/env bash

# get the absolute path of the directory this script is in (can include links)
THIS_DIR=$(dirname $(realpath --no-symlinks "${BASH_SOURCE}"))

source "${THIS_DIR}/_get_proj_vars.sh"

BUILD_CONFIG="native_dbg"
OPTS="-DVIDEO_MIR=OFF -DURHO3D_PCH=1"
OPTS="${OPTS} -DCMAKE_BUILD_TYPE=Debug -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON"
OPTS="${OPTS} $@"

source ${THIS_DIR}/_build_common.sh

