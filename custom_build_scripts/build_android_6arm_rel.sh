#!/usr/bin/env bash

# get the absolute path of the directory this script is in (can include links)
THIS_DIR=$(dirname $(realpath --no-symlinks "${BASH_SOURCE}"))

source "${THIS_DIR}/_get_proj_vars.sh"

BUILD_CONFIG="android_6arm_rel"
ANDROID_TARGET=1
#BUILD_APK=1
OPTS="-DANDROID_ABI=armeabi-v7a -DANDROID_NATIVE_API_LEVEL=android-23"
OPTS="${OPTS} -DANDROID_TOOLCHAIN_NAME=arm-linux-androideabi-4.9"
OPTS="${OPTS} -DANDROID_STL=gnustl_shared -DANDROID_RTTI:BOOL=ON"
OPTS="${OPTS} -DANDROID_EXCEPTIONS:BOOL=ON -DANDROID_NOEXECSTACK:BOOL=ON"
OPTS="${OPTS} -DANDROID_RELRO:BOOL=ON -DANDROID_FORMAT_SECURITY:BOOL=ON"
OPTS="${OPTS} -DANDROID_FUNCTION_LEVEL_LINKING:BOOL=ON -DVIDEO_MIR=0"
OPTS="${OPTS} -DURHO3D_PCH=1 -DCMAKE_BUILD_TYPE=Release"
OPTS="${OPTS} -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON"
OPTS="${OPTS} $@"

source ${THIS_DIR}/_build_common.sh
