#!/usr/bin/env bash

# get the absolute path of the directory this script is in (can include links)
THIS_DIR=$(dirname $(realpath --no-symlinks "${BASH_SOURCE}"))

source "${THIS_DIR}/_get_proj_vars.sh"

BUILD_FULL_DIR=$(readlink -f ${BASE_DIR}/build)

cd ${BUILD_FULL_DIR} && \
          make clean && \
          ${SOURCE_DIR}/cmake_clean.sh ${BUILD_FULL_DIR}

#if [ -d ${BUILD_DIR} -a -f ${BUILD_DIR}/CMakeCache.txt ]; then
#  rm -rf ${BUILD_DIR}/*
#  touch ${SOURCE_DIR}/last_build.lnk/CMakeCache.txt
#else
#  echo "Build directory \"$BUILD_DIR\" does not exist or does not have a CMakeCache.txt - nothing to do"
#fi
