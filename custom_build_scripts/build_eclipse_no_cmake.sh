#!/usr/bin/env bash

# get the absolute path of the directory this script is in (can include links)
THIS_DIR=$(dirname $(realpath --no-symlinks "${BASH_SOURCE}"))

source "${THIS_DIR}/_get_proj_vars.sh"

BUILD_CONFIG="eclipse"
SKIP_CMAKE=1

source ${THIS_DIR}/_build_common.sh
