
#!/usr/bin/env bash

# this should be sourced to do the common part of the build
#
# Preconditions:
# - the following variables should be defined:
#   * SOURCE_DIR:   the source directory
#   * BASE_DIR:     the base directory of the project, where builds and source
#                   dirs are located
#   * THIS_DIR:     the directory where this and parent scripts are located
#   * BUILD_CONFIG: with a unique name for the intended configuration
#   * OPTS:         options to pass to cmake

BUILD_DIR="build_${BUILD_CONFIG}"
BUILD_FULL_DIR="${BASE_DIR}/${BUILD_DIR}"

# setup output dir
cd ${BASE_DIR}
mkdir -p ${BUILD_DIR}
ln -sfn ${BUILD_DIR} build
cd ${THIS_DIR}/..

android_update()
{
  if [ $ANDROID_TARGET ]; then
    banner "ANDROID=${ANDROID_TARGET}"
    # android cmd does not return bad status code on failure. Following tricks
    # try to deal with that.
    exec 5>&1 # redirect anything that appears in file descriptor 5 to std out
    # execute android update, capturing std err in variable but still echoing
    err=$( (android update project        \
           -s -p . -t ${ANDROID_TARGET} ) \
           2>&1 | tee /dev/fd/5)
    exec 5>&- # cancel redirection of fd 5 to std out
    # if the lower case version of err contains the string "error", we failed
    if [[ "${err,,}" == *"error"* ]]; then
      return 1
    fi
  fi
}

android_ant_debug()
{
  if [ $ANDROID_TARGET ] && [ $BUILD_APK ]; then
    banner "ANT DBG"
    ant debug
  fi
}

if [ $SKIP_CMAKE ]; then
  cmake_script='echo "Skipping cmake" '
elif [[ $BUILD_CONFIG == *"eclipse"* ]]; then
  cmake_script=${SOURCE_DIR}/cmake_eclipse.sh
elif [ $ANDROID_TARGET ]; then
  cmake_script=${SOURCE_DIR}/cmake_android.sh
else
  cmake_script=${SOURCE_DIR}/cmake_generic.sh
fi

run_make()
{
  banner "building" && tmake && android_ant_debug
}

buildit()
{
  echo ${cmake_script} "${BUILD_FULL_DIR}" ${OPTS} "$@"
  eval ${cmake_script} "${BUILD_FULL_DIR}" ${OPTS} "$@" \
                     && cd ${BUILD_FULL_DIR}            \
                     && android_update                  \
                     && run_make
}

ban buildit |& tee cmake.log
